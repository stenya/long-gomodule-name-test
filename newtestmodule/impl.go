package newtestmodule

// GetHelloWorldTM returns "Hello world"
func GetHelloWorldTM() string {
	return "Hello world TM"
}

// GetNewHelloWorldTM returns "Hello world"
func GetNewHelloWorldTM() string {
	return GetHelloWorldTM()
}
